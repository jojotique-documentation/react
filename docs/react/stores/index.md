# React et la gestion de données centralisée

React est basé sur le principe de composants. Pour passer une information d'un composant à un autre, on peut soit
utiliser les `props` pour faire descendre les données, soit des `callbacks` pour faire remonter les données.

Cependant, pour des applications plus complexes, cette méthode peut devenir rapidement compliquée. C'est là qu'
intervient la gestion de données centralisée.

## Les stores

Un store est un objet qui contient l'état de l'application. Il est accessible par tous les composants de l'application.
Il va permettre de partager des données et des fonctions entre les composants.

Il existe plusieurs librairies pour gérer les stores. La plus connue est Redux, mais il en existe d'autres comme MobX.

React propose aussi un système de gestion de données centralisée avec les providers et les contextes.

### Redux

Redux est une librairie qui permet de gérer l'état de l'application. Il est basé sur trois principes :

- Un seul état : l'état de l'application est stocké dans un seul objet.
- L'état est immuable : pour modifier l'état, on crée un nouvel objet.
- Les modifications de l'état sont prévisibles : on utilise des actions pour modifier l'état.

### Les providers et les contextes

React propose un système de gestion de données centralisée avec les providers et les contextes. Un provider est un
composant qui va fournir des données à ses enfants via un contexte.
