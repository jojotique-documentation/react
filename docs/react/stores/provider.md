# Les providers et les contextes

## Définitions

### Provider

En react, un provider est un composant qui permet de fournir des données à ses enfants. Il est utilisé en conjonction
avec le contexte.

### Contexte

Le contexte est un moyen de partager des données entre des composants sans avoir à passer explicitement des props à
travers chaque niveau de l'arborescence. Il est souvent utilisé pour fournir des données qui sont considérées comme des
données globales pour l'application, telles que le thème ou l'utilisateur connecté.

## Utilisation

### Création d'un context

Pour créer un contexte, on utilise la méthode `createContext` de la librairie `react`.

```tsx
import { createContext } from 'react';

interface SecurityContext {
  user: User | null;

  setUser: (user: User | null) => void;
}

const SecurityContext = createContext<SecurityContext | undefined>();
```

### Utilisation d'un context

Pour utiliser un contexte, on utilise le composant `useContext` de la librairie `react` et on l'exporte dans un hook
pour l'utiliser plus facilement dans les composants enfants.

```tsx
import { createContext } from 'react';

interface SecurityContext {
  user: User | null;

  setUser: (user: User | null) => void;
}

const SecurityContext = createContext<SecurityContext | undefined>();

export const useSecurity = () => {  // [!code focus]
  const context = useContext(SecurityContext); // [!code focus]

  if (!context) { // [!code focus]
    throw new Error('useSecurity must be used within a SecurityProvider'); // [!code focus]
  } // [!code focus]

  return context; // [!code focus]
}; // [!code focus]
```

... dans le composant enfant

```tsx
import { useSecurity } from './SecurityContext';

const MyComponent = () => {
  const { user, setUser } = useSecurity();

  return (
    <div>
      <p>{user?.name}</p>
      <button onClick={() => setUser(null)}>Logout</button>
    </div>
  );
};
```

### Création d'un provider

Le context doit être fourni par un provider. Pour créer un provider, on utilise le composant `Provider` de la librairie `react`.

```tsx
import type { FC, ReactNode } from 'react'; // [!code focus]
import { createContext } from 'react'; // [!code --] // [!code focus]
import { createContext, useState } from 'react'; // [!code ++] // [!code focus]

interface SecurityContext {
  user: User | null;

  setUser: (user: User | null) => void;
}

const SecurityContext = createContext<SecurityContext | undefined>();

export const useSecurity = () => { 
  const context = useContext(SecurityContext);

  if (!context) {
    throw new Error('useSecurity must be used within a SecurityProvider');
  }

  return context;
};

export const SecurityProvider: FC<{ children?: ReactNode }> = ({ children }) => { // [!code focus]
  const [user, setUser] = useState<User | null>(null); // [!code focus]

  return ( // [!code focus]
    <SecurityContext.Provider value={{ user, setUser }}> // [!code focus]
      {children} // [!code focus]
    </SecurityContext.Provider> // [!code focus]
  ); // [!code focus]
}; // [!code focus]
```

Puis, on doit ajouter le `SecurityProvider` dans l'arborescence des composants, au niveau le plus haut.

```tsx
import { SecurityProvider } from './SecurityContext';

const App = () => {
  return (
    <SecurityProvider>
      <MyComponent />
    </SecurityProvider>
  );
};
```