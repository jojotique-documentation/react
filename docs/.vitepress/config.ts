import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: 'React',
  description: 'Site de veille sur React',

  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Accueil', link: '/' },
      { text: 'React', link: '/react/introduction/' },
      { text: 'React-Router', link: '/react-router/introduction/' },
    ],

    sidebar: {
      '/react/': [
        {
          text: 'Introduction',
          items: [
            { text: 'React', link: '/react/introduction/' },
          ],
        },
        {
          text: 'Stores',
          link: '/react/stores/',
          items: [
            { text: 'Redux', link: '/react/stores/redux' },
            { text: 'Provider', link: '/react/stores/provider' },
          ],
        },
      ],
      '/react-router/': [
        {
          text: 'Introduction',
          items: [
            { text: 'React-Router', link: '/react-router/introduction/' },
          ],
        },
        {
          text: 'Routes',
          items: [
            { text: 'Route', link: '/react-router/routes/route' },
            { text: 'Route authentifiée', link: '/react-router/routes/routeAuthenticated' },
            { text: 'Paramètres', link: '/react-router/routes/params' },
          ],
        },
        {
          text: 'Navigation',
          items: [
            { text: 'Les liens', link: '/react-router/navigation/link' },
          ],
        },
      ],
    },

    socialLinks: [],
    outline: {
      level: [2, 4],
    },
  },

  // Gitlab Pages deployment
  outDir: '../public',
  base: '/react/',

  lang: 'fr-FR',
})
