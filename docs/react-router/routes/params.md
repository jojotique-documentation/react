# Les paramètres

## Création d'une route avec des paramètres

Les paramètres sont des éléments de l'URL qui sont utilisés pour capturer des valeurs dynamiques. Les paramètres sont
définis dans l'URL en utilisant `:` suivi du nom du paramètre. Par exemple, dans la route `/users/:id`, `:id` est un
paramètre qui représente l'identifiant de l'utilisateur.

Exemple, création d'un routeur avec une route avec des paramètres :

```tsx
import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

const App = () => {
  return (
      <Router>
        <div>
          <ul>
            <li><Link to="/user/bob">Bob</Link></li>
            <li><Link to="/user/jeff">Jeff</Link></li>
          </ul>
          <Route path="/user/:username" component={User}/>
        </div>
      </Router>
  );
};
```

## Récupération des paramètres

Pour récupérer les paramètres, on doit utiliser le hook `useParams` de `react-router-dom`. Ce hook retourne un objet
avec les paramètres de l'URL.

Exemple, récupération des paramètres :

```tsx
import React from 'react';
import { useParams } from 'react-router-dom';

const User = () => {
  let { username } = useParams();
  return <h1>Hello {username}!</h1>;
};
```